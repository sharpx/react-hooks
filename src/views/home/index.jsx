import React from 'react'
import { Link } from 'react-router-dom'
function Home() {
  return (
    <>
      <h1>home page</h1>
      <div>
        <Link to='/hooks'>hooks</Link>
      </div>
    </>
  )
}
export default Home
import React, { useState } from 'react'
// 使用场景 简单
function UseState() {
  const [count, setCount] = useState(0)
  const add = () => {
    setCount(count + 1)
  }
  const del = () => {
    setCount(count - 1)
  }
  return (
    <>
      <h1>useState</h1>
      <h1>{count}</h1>
      <div>
        <div onClick={add}>add</div>
        <div onClick={del}>del</div>
      </div>
    </>
  )
}
export default UseState
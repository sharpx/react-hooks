import React, { createContext } from 'react'
import UseContextChild from './useContextChild'
const ctx = createContext();
// context 子组件共享状态
function App() {
  return <ctx.Provider value={{ count: 200, name: 'sharp' }} >
    <UseContextChild context={ctx}></UseContextChild>
  </ctx.Provider>
}

export default App;
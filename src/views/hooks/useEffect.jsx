import React, { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
function UseState() {
  const [count, setCount] = useState(0)
  const [num, setNum] = useState(100)
  const add = () => {
    setCount(count + 1)
  }
  const addNum = () => {
    setNum(num + 1)
  }
  const del = () => {
    setCount(count - 1)
  }
  useEffect(() => {
    console.log('effect', count)
    return () => {
      // 组件销毁时执行
      console.log('destroy')
    }
  }, [count]) // 只有数组中的值发生变化才会执行
  return (
    <>
      <h1>useState</h1>
      <h1>{count}</h1>
      <div>
        <div onClick={add}>add</div>
        <div onClick={addNum}>addNum</div>
        <div onClick={del}>del</div>
      </div>
    </>
  )
}
export default UseState
import React from 'react'
import { Link } from 'react-router-dom'
function Hook() {
  return (
    <>
      <h1>hooks</h1>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <Link to='/hooks/useState'>useState</Link>
        <Link to='/hooks/useEffect'>useEffect</Link>
        <Link to='/hooks/useReducer'>useReducer</Link>
        <Link to='/hooks/useMemo'>useMemo</Link>
        <Link to='/hooks/useCallback'>useCallback</Link>
        <Link to='/hooks/useContext'>useContext</Link>
      </div>
    </>
  )
}
export default Hook
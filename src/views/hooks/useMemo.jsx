import React, { useState, useMemo } from 'react'
function UseMemo() {
  const [count, setCount] = useState(0)
  const add = () => {
    setCount(count + 1)
  }
  const del = () => {
    setCount(count - 1)
  }
  const getCount = () => {
    console.log('getCount', count)
    return count
  }
  useMemo(() => {
    console.log('getCount_memo', count)
    return count
  }, [count]) // 只有数组中的变量发生改变才执行
  return (
    <>
      <h1>useState</h1>
      <div>
        <div onClick={add}>add</div>
        <div onClick={del}>del</div>
        <div onClick={getCount}>getCount</div>
      </div>
    </>
  )
}
export default UseMemo
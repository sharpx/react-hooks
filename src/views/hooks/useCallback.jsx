import React, { useState, memo, useCallback } from 'react'

/***
 * useCallback 缓存方法 父组件更新不会引起子组件的更新
 * memo 缓存组件本身 类似React.pureComponent
 *  */

const Child = memo((props) => {
  console.log(props, 'child');

  return (
    <div>
      <input type="text" onChange={props.onChange} />
    </div>
  )
})

const Parent = () => {
  console.log('parent')
  const [count, setCount] = useState(0)
  const [text, setText] = useState('')

  const handleOnChange = useCallback((e) => {
    setText(e.target.value)
  }, [])

  // const handleOnChange = (e) => {
  //   setText(e.target.value)
  // }

  return (
    <div>
      <div>count: {count}</div>
      <div>text: {text}</div>
      <button onClick={() => {
        setCount(count + 1)
      }}>+1</button>
      <Child onChange={handleOnChange} />
    </div>
  )
}

function App() {
  return <div><Parent /></div>
}

export default App
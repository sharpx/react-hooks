import React, { useReducer } from 'react'
// 使用场景 复杂
function UseState() {
  const [count, dispatch] = useReducer((state, action) => {
    switch (action) {
      case 'add':
        return state + 1;
      case 'del':
        return state - 1;
      default:
        return state;
    }
  }, 100)
  return (
    <>
      <h1>useState</h1>
      <h1>{count}</h1>
      <div>
        <div onClick={() => dispatch('add')}>add</div>
        <div onClick={() => dispatch('del')}>del</div>
      </div>
    </>
  )
}
export default UseState
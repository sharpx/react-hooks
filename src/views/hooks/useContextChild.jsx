import React, { useContext } from 'react'
function App(props) {
  const { count, name } = useContext(props.context)
  return (
    <>
      <div>{count}--child</div>
      <div>{name}--child</div>
    </>
  )
}

export default App;
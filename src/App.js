import React, { Suspense } from 'react'
import 'antd/dist/antd.less';
import './App.less';
import { Switch, Route, HashRouter as Router } from 'react-router-dom'
import routers from './routers'
function App() {
  return (
    <Router>
      <Suspense fallback={<div>加载中...</div>}>
        <Switch>
          {
            routers.map((route, i) => {
              return <Route
                path={route.path}
                key={i}
                exact={route.exact}
                render={props => {
                  return <route.component {...props} />
                }}
              />
            })
          }
        </Switch>
      </Suspense>
    </Router>
  );
}

export default App;

import { lazy } from 'react'

const loadable = (path) => {
  return lazy(() => import(`@/views/${path}`))
}

const routers = [
  {
    path: '/',
    exact: true,
    component: loadable('home')
  },
  {
    path: '/hooks',
    exact: true,
    component: loadable('hooks')
  },
  {
    path: '/hooks/useState',
    exact: true,
    component: loadable('hooks/useState')
  },
  {
    path: '/hooks/useEffect',
    exact: true,
    component: loadable('hooks/useEffect')
  },
  {
    path: '/hooks/useReducer',
    exact: true,
    component: loadable('hooks/useReducer')
  },
  {
    path: '/hooks/useMemo',
    exact: true,
    component: loadable('hooks/useMemo')
  },
  {
    path: '/hooks/useCallback',
    exact: true,
    component: loadable('hooks/useCallback')
  },
  {
    path: '/hooks/useContext',
    exact: true,
    component: loadable('hooks/useContext')
  }
]

export default routers
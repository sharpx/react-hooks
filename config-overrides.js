const {
  override,
  fixBabelImports,
  addLessLoader,
  addWebpackAlias,
  addWebpackPlugin,
} = require('customize-cra')

const path = require('path')
const paths = require('react-scripts/config/paths')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

/**
 * 生产环境是否打包 Source Map 两种方法
 *
 */
const rewiredMap = () => config => {
  config.devtool = config.mode === 'development' ? 'cheap-module-source-map' : false

  return config
}
process.env.GENERATE_SOURCEMAP !== 'false'

console.log(process.env.NODE_ENV)

// path
const resolveAlias = dir => path.join(__dirname, '.', dir)

// build--->prod --->文件设置
const appBuildPathFile = () => config => {
  if (config.mode === 'development') {
    console.log('evn is development, skip build path change...')
  } else if (config.mode === 'production') {
    console.log('evn is production, change build path...')
    // 关闭sourceMap
    config.devtool = false
    //  // 配置打包后的文件位置修改path目录
    // paths.appBuild = path.join(path.dirname(paths.appBuild), 'dist')
    // config.output.path = path.join(path.dirname(config.output.path), 'dist')
  }
  return config
}

module.exports = {
  webpack: override(
    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true
    }),
    addLessLoader({
      modifyVars: {
        "@primary-color": "#1DA57A", // for example, you use Ant Design to change theme color.
      },
      javascriptEnabled: true,
    }),
    addWebpackAlias({
      '@': resolveAlias('src'),
    }),
    // addWebpackPlugin(new BundleAnalyzerPlugin()),
  ),
  paths: (paths, env) => {
    return paths
  }
}